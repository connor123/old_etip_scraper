from django.db import models

class Client(models.Model):
	name = models.CharField(max_length=150)
	description = models.CharField(max_length=150, null= True, blank=True)
	

	def __str__(self):
		return self.name