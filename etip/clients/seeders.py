from clients.models import Client

def seed_one_client(**kwargs):
    return Client.objects.create(**kwargs)