from django.test import TestCase
from clients.models import Client
from clients.seeders import seed_one_client

class ClientTest(TestCase):

	def test_client_exists(self):
		client_db = seed_one_client(name="test")
		client, created = Client.objects.get_or_create(name="test")

		self.assertTrue(client == client_db, "client doesnt exist")
		self.assertFalse(created, "client doesnt exist")
	

	def test_client_not_exists(self):
		client_db = seed_one_client(name="test")
		client, created = Client.objects.get_or_create(name="new client")

		self.assertTrue(client != client_db, "clients shouldnt be the same")
		self.assertTrue(created, "client doesnt exist")
