import time
import sys
from selenium import webdriver
from shifts.models import Shift
from datetime import datetime
import requests
from shifts.helpers import get_dt

#startTime = datetime.now()
#############################LOG IN TO SITE ########################################
def logIn(userin="nursing.southwest@pulsejobs.com",
			passin="Turnford1"):
	try:

		# the victim site we're scraping
		url = "https://e-tips.com/auth/login"

		# Select the browser for selenium to use
		driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
		driver.get(url)

		# locate username and password fields and input login details
		username = driver.find_element_by_xpath("//input[@placeholder='Email']")
		password = driver.find_element_by_xpath("//input[@placeholder='Password']")

		# While we are on a page with a Sign In button try to sign in
		#while driver.find_elements_by_xpath("//*[contains(text(), 'Sign In')]")[0]:
		username.click()
		username.send_keys(userin)
		password.click()
		password.send_keys(passin)
		time.sleep(1)
		# locate and click "Sign In"
		driver.find_elements_by_xpath("//*[contains(text(), 'Sign In')]")[0].click()

		# Return driver
		navigate(driver)
		
	# General error handling
	except Exception as e:
		print(e)
		raise e

########################NAVIGATE TO VACANCIES PAGE ########################################
def navigate(driver):

	try:
		# wait for page to load
		time.sleep(2)
		# nav to vacancies
		driver.get("https://e-tips.com/vacancies")
		display_100(driver)
		scrape(driver)

	# General error handling	
	except Exception as e:
		print(e)
		raise e

#########################SCRAPE THE DATA!!!################################################
def scrape(driver):
	from clients.models import Client
	shift_data = []
	raw_data = []
	try:

		#get_dt(driver)
		while len(raw_data)==0:
			time.sleep(1)
			raw_data = driver.find_elements_by_class_name("grid__data-row")
		
		for element in raw_data:
			info = element.text
			#print(info)
			info = [s.strip() for s in info.splitlines()]
			if len(info) != 15:
				print("Bad: " + str(info))
				print("info length: " + str(len(info)))
				if info[2] == info[3]:
					del info[2]
				
			else:
				print("Good: " + str(info))

			client, created = Client.objects.get_or_create(name = info[3], description = info[4])  
			new_start_dt = get_dt(info[6])
			new_end_dt = get_dt(info[7])
			
			dict_info = {
				"ref": info[1],
				"business_unit": info[2],
				"client": client,
				"job": info[5],
			#	"start_dt": info[6],
				"start_dt": new_start_dt,
				"end_dt": new_end_dt,
			#	"end_dt": info[7],
				"required": info[8],
			}	
			shift_data.append(dict_info)
			

			#for i in shift_data:
			#	print(i)
			for shift_info in shift_data:
			    try:
			        Shift.objects.create(**shift_info)
			    except Exception as e:
			        print(e)

		page_turn(driver)

	# General error handling
	except Exception as e:
		print(e)
		raise e

##############################TURN THE PAGE##############################################
def page_turn(driver):
	# finds the footer element where the page turn button lives
	pagenum = driver.find_elements_by_class_name("pull-left")
	pagenum = pagenum[0].text # returns >> "Showing 3 of 23 pages    records per page"
	pagenum = pagenum.split(" ")#			[0]    [1][2][3][4]       [5]    [6] [7]

	# checks if we're on the last page or not
	if pagenum[1] == pagenum[3]:
		print("scrape is done!")
		#print(datetime.now() - startTime) uncomment to record scrape time
		return

	else:
		next_page = driver.find_elements_by_class_name("grid__paginator-next-page")[0].click()
		time.sleep(1)
		print("######################   page " + pagenum[1] + " scraped! ########################################")
		time.sleep(1)
		scrape(driver)
		
########################### DISPLAY 100 RESULTS PER PAGE #################################
def display_100(driver):
	# clicks the drop down menu
	drop_down = driver.find_element_by_xpath("//div[@class='btn-group']").click()

	# selects the "100" results per page on the drop down menu
	click_100 = driver.find_element_by_link_text("100").click()
	return(driver)
	
