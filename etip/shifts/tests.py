from django.test import TestCase
from shifts.models import Shift
from shifts.seeders import seed_shift
from clients.models import Client
from clients.seeders import seed_one_client


class ShiftTest(TestCase):

	def setUp(self):
		
		client, created = Client.objects.get_or_create(name="test")
		self.template = {
			'ref':"test",
			'start_dt': "2018-07-13 14:56",
			'end_dt': "2018-08-14T06:30:00Z", 
			'required': 1, 
			'job': "Surgeon",  
			'business_unit': "endoscopy", 
			'client': client
		}

	def test_shift_insertions(self):

		#shift_db = seed_shift(ref="test", start_dt ="2018-07-13 14:56", end_dt ="2018-07-13 21:56", required = 1)
		test_shift, created = Shift.objects.get_or_create(**self.template)
		for key, item in self.template.items():
			self.assertEqual(item, getattr(test_shift, key), "failed to test %s" % key)




	def test_shift_insertions_fails(self):
		test_shift_fail, created = Shift.objects.get_or_create(**self.template)
		for key, item in self.template.items():
			self.assertNotEqual(item, getattr(test_shift_fail, key), "failed to test %s" % key)