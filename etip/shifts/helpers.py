import re
from datetime import datetime
import pytz

def get_dt(shift_time):
	try:

		cur_year = datetime.now().year
		cur_month = datetime.now().month
		words = str(shift_time).split()
		day = words[0]
		r = re.compile("([0-9]+)([a-zA-Z]+)")
		m = r.match(day)
		day2 = m.group(1)
		month = datetime.strptime(words[1], "%b")
		month2 = str(month).split("-")
		month3 = month2[1]
		if int(month3) < int(cur_month):
			cur_year = cur_year + 1
		time = words[2].split(":")
		hour = time[0]
		minute = time[1]
		new_dt = str(cur_year) + " " + str(month3) + " " + str(day2) + " " + str(hour) + ":" + str(minute)# + "tzinfo=pytz.UTC"
		dt_obj = datetime.strptime(new_dt, "%Y %m %d %H:%M")
		return dt_obj
	except Exception as e:
		print(e)


# import re
# from datetime import datetime
# import pytz

# def get_dt(shift_time):
# 	try:

# 		cur_year = datetime.now().year
# 		cur_month = datetime.now().month
# 		words = str(shift_time).split()
# 		day = words[0]
# 		r = re.compile("([0-9]+)([a-zA-Z]+)")
# 		m = r.match(day)
# 		day = m.group(1)
# 		month = datetime.strptime(words[1], "%b")
# 		month = str(month).split("-")
# 		month = month[1]
# 		if int(month) < int(cur_month):
# 			cur_year = cur_year + 1
# 		time = words[2].split(":")
# 		hour = time[0]
# 		minute = time[1]
# 		new_dt = str(cur_year) + " " + str(month) + " " + str(day) + " " + str(hour) + ":" + str(minute)# + "tzinfo=pytz.UTC"
# 		dt_obj = datetime.strptime(new_dt, "%Y %m %d %H:%M")
# 		print(dt_obj)
# 		return dt_obj
# 	except Exception as e:
# 		print(e)




