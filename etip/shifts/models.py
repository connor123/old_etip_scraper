from django.db import models
from clients.models import Client

class Shift(models.Model):
	ref = models.CharField(max_length=100)
	business_unit = models.CharField(max_length=100)
	client = models.ForeignKey(Client, related_name="shifts", null=True, on_delete=models.SET_NULL)
	job = models.CharField(max_length=100)
	start_dt = models.DateTimeField()
	end_dt = models.DateTimeField()
	required = models.IntegerField()

	def __str__(self):
		return self.business_unit


