from django.core.management.base import BaseCommand
import os
from shifts.scrapes import logIn

class Command(BaseCommand):
    help = 'Runs the scraper'

    def handle(self, **options):
    	logIn()
        #os.system("python ~/Desktop/etipscrape/etip/shifts/scrapes.py")
