from shifts.models import Shift

def seed_shift(**kwargs):
    return Shift.objects.create(**kwargs)
